from setuptools import setup, find_packages

setup(
    name='isnet',
    version='0.1',
    packages=find_packages(),
    description='Foreground-background decomposition',
    url='https://gitlab.com/alan.aboudib/dis/',
    author='Alan Aboudib',
    author_email='agabudeeb@gmail.com',
    install_requires=[ # Versions should be ideally specified with exact version numbers
        'torch==1.13.1',
        'torchvision==0.14.1',
        'opencv-python',
        'gdown',
        'scikit-image',
    ],
)
