from segment_foreground import decompose_image

image_path = '/home/alan/deeplearning/ocus/hire_test/ocus008.jpg'

fg_image, bg_image, mask = decompose_image(image_path)

print(fg_image)
bg_image.save('./img.png')
mask.save('./mask.png')
