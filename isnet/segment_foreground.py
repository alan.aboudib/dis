# Library imports
import numpy as np
from PIL import Image
from PIL import ImageFilter
import cv2
import torch
from torch.autograd import Variable
from torchvision import transforms
import torch.nn.functional as F
import gdown
import os

# Project imports
from .data_loader_cache import normalize, im_reader, im_preprocess
from .models import ISNetDIS


def get_hyperparams():
    """Define a dict with the basic hyperparamters of the model
    """

    hyper = {} # paramters for inferencing


    hyper["model_path"] ="./dis_saved_models" ## load trained weights from this path
    hyper["restore_model"] = "isnet.pth" ## name of the to-be-loaded weights
    hyper["interm_sup"] = False ## indicate if activate intermediate feature supervision

    ##  choose floating point accuracy --
    hyper["model_digit"] = "full" ## indicates "half" or "full" accuracy of float number
    hyper["seed"] = 0

    hyper["cache_size"] = [1024, 1024] ## cached input spatial resolution, can be configured into different size

    ## data augmentation parameters ---
    hyper["input_size"] = [1024, 1024] ## mdoel input spatial size, usually use the same value hyper["cache_size"], which means we don't further resize the images
    hyper["crop_size"] = [1024, 1024] ## random crop size from the input, it is usually set as smaller than hyper["cache_size"], e.g., [920,920] for data augmentation

    hyper["model"] = ISNetDIS()

    return hyper

class GOSNormalize(object):
    '''
    Normalize the Image using torch.transforms
    '''
    def __init__(self, mean=[0.485,0.456,0.406], std=[0.229,0.224,0.225]):
        self.mean = mean
        self.std = std

    def __call__(self,image):
        image = normalize(image,self.mean,self.std)
        return image



def load_image(im_path, hyper):
    """Prepare image for being consumed by the model
    """
    
    if im_path.startswith("http"):
        im_path = BytesIO(requests.get(im_path).content)

    im = im_reader(im_path)
    im, im_shp = im_preprocess(im, hyper["cache_size"])
    im = torch.divide(im,255.0)
    shape = torch.from_numpy(np.array(im_shp))

    # Define the transforms
    transform =  transforms.Compose([GOSNormalize([0.5,0.5,0.5],[1.0,1.0,1.0])])
    
    return transform(im).unsqueeze(0), shape.unsqueeze(0) # make a batch of image, shape

def build_model(hyper,device):
    net = hyper["model"]

    # convert to half precision
    if(hyper["model_digit"]=="half"):
        net.half()
        for layer in net.modules():
            if isinstance(layer, nn.BatchNorm2d):
                layer.float()

    net.to(device)

    if(hyper["restore_model"]!=""):
        net.load_state_dict(torch.load(hyper["model_path"]+"/"+hyper["restore_model"],map_location=device))
        net.to(device)
    net.eval()

    
    return net

def download_checkpoint():
    """Download model checkpoint.
    """

    # Download official weights
    if not os.path.exists("./dis_saved_models"):

        print("Downloading the model's checkpoint ...")
        
        os.mkdir('./dis_saved_models')

        MODEL_PATH_URL = "https://drive.google.com/uc?id=1KyMpRjewZdyYfxHPYcd-ZbanIXtin0Sn"

        gdown.download(MODEL_PATH_URL, "./dis_saved_models/isnet.pth", use_cookies=False)

    print("model's checkkpoint saved.")

def segment_foreground(image_path, net, hyper, device):
    image_tensor, orig_size = load_image(image_path, hyper)
    
    mask = predict(net,image_tensor,orig_size, hyper, device)

    mask = Image.fromarray(mask.astype(np.uint8))
    
    return mask

def get_fg_image(original, mask):
  """Remove the background from the original image
  """

  # Load the image and the mask as numpy arrays
  original = np.array(original)
  mask = np.array(mask)

  # Ensure the mask is binary (i.e., contains only 0 and 1, or 0 and 255)
  mask = mask / 255

  # Add an additional dimension to the mask
  mask_rgb = np.stack([mask]*3, axis=-1)

  # Create the masked image
  masked_image = original * mask_rgb

  # Save the masked image
  result = Image.fromarray(masked_image.astype(np.uint8))



  return result

def get_bg_image(original, mask):
  """Remove the foreground from the original image
  """

  # Load the image and the mask as numpy arrays
  original = np.array(original)
  mask = np.array(mask)

  # Ensure the mask is binary (i.e., contains only 0 and 1, or 0 and 255)
  mask = 1- mask / 255

  # Add an additional dimension to the mask
  mask_rgb = np.stack([mask]*3, axis=-1)

  # Create the masked image
  masked_image = original * mask_rgb

  # Save the masked image
  result = Image.fromarray(masked_image.astype(np.uint8))



  return result


def predict(net,  inputs_val, shapes_val, hyper, device):
    '''
    Given an Image, predict the mask
    '''
    net.eval()

    if(hyper["model_digit"]=="full"):
        inputs_val = inputs_val.type(torch.FloatTensor)
    else:
        inputs_val = inputs_val.type(torch.HalfTensor)


    inputs_val_v = Variable(inputs_val, requires_grad=False).to(device) # wrap inputs in Variable

    ds_val = net(inputs_val_v)[0] # list of 6 results

    pred_val = ds_val[0][0,:,:,:] # B x 1 x H x W    # we want the first one which is the most accurate prediction

    ## recover the prediction spatial size to the orignal image size
    pred_val = torch.squeeze(F.upsample(torch.unsqueeze(pred_val,0),(shapes_val[0][0],shapes_val[0][1]),mode='bilinear'))

    ma = torch.max(pred_val)
    mi = torch.min(pred_val)
    pred_val = (pred_val-mi)/(ma-mi) # max = 1

    if device == 'cuda': torch.cuda.empty_cache()
    return (pred_val.detach().cpu().numpy()*255).astype(np.uint8) # it is the mask we need



def decompose_image(image_path:str):
    """Decompose and image into a foreground, background,
    a mask
    """

    # Download the model's checkpoint.
    download_checkpoint()
    
    # Get hyperparamters
    hyper = get_hyperparams()

    # Get the device
    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    net = build_model(hyper, device)

    mask = segment_foreground(image_path,
                              net = net,
                              hyper = hyper,
                              device = device)

    img = Image.open(image_path)

    fg_image = get_fg_image(original = img, mask = mask)
    bg_image = get_bg_image(original = img, mask = mask)

    return fg_image, bg_image, mask

